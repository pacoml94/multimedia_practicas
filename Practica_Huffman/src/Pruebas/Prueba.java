package Pruebas;

import java.awt.List;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import entrada_salida.EscritorBinario;
import entrada_salida.LectorBinario;
import estructuras_datos.ArbolHuffman;

class Prueba {

	public static void main(String[] args) {
		LectorBinario lector = 
				new LectorBinario("/Users/franciscomurillolopez/git/multimedia_practicas/Practica_Huffman/prueba_para_comprimir.txt");
		// Leer archivo de entrada y almacenar en una cadena
		StringBuilder sb = new StringBuilder();
		while (!lector.esVacio()) {
			char b = lector.leerPalabra();
			sb.append(b); 	// OJO! leerPalabra() devuelve una palabra 
							// de 8 bits y el tipo char es de 16 bits
		}
		char[] input = sb.toString().toCharArray();

		/////////////////////// TAREA1.1///////////////////////
		// Generar tabla de frecuencias (freq) a partir del array de tipo char input.
		Map<Character, Integer> mapCharacteres = new HashMap<Character, Integer>();
		int[] freq = new int[256];
		for (int i = 0; i < freq.length; i++) {
			freq[i] = 0;
		}
		for (int i = 0; i < input.length; i++) {
			freq[(int) input[i]] += 1;
		}
		
		 for (int i = 0; i < freq.length; i++) {
				if (freq[i] != 0) {
					mapCharacteres.put((char) i, freq[i]);
				}
			}
		 //System.out.println(mapCharacteres.toString());
		 ArbolHuffman arbolHuffman = construirArbol(mapCharacteres);
		//System.out.println(arbolHuffman.toString());
		Map<Character, String> diccionarioCodigos = new HashMap<>();
		 construirCodigos(diccionarioCodigos, arbolHuffman, "");
		 System.out.println(diccionarioCodigos.toString());
		 codificar(input, diccionarioCodigos, "/Users/franciscomurillolopez/git/multimedia_practicas/Practica_Huffman/compresion.txt", arbolHuffman);
	}
	
	private static ArbolHuffman construirArbol(Map<Character,Integer> freq) {
		PriorityQueue<ArbolHuffman> cola = new PriorityQueue<>();
		Iterator<Character> it = freq.keySet().iterator();
		while (it.hasNext()) {
			Character letra = (Character) it.next();
			cola.add(new ArbolHuffman(letra, freq.get(letra), null, null));
		}
		
		ArbolHuffman arbolHuffman = null;
		while (cola.size() != 1) {
			ArbolHuffman aux1, aux2;
			aux1 = cola.poll();
			aux2 = cola.poll();
			int frecTotal = aux1.getFrecuencia() + aux2.getFrecuencia();
			arbolHuffman = new ArbolHuffman('\0', frecTotal, aux1, aux2);
			cola.add(arbolHuffman);
		}
		
		return arbolHuffman;
 	}
	
	 private static void construirCodigos(Map<Character,String>  diccionarioCodigos, ArbolHuffman arbol,String codigoCamino){
 		///////////////////////TAREA1.5///////////////////////
 		
			if(arbol.getDerecho()!=null) {
				construirCodigos(diccionarioCodigos, arbol.getDerecho(), codigoCamino+"1");
			}
			if(arbol.getIzquierdo()!=null) {
				construirCodigos(diccionarioCodigos, arbol.getIzquierdo(), codigoCamino+"0");
			} else {
				diccionarioCodigos.put(arbol.getSimbolo(), codigoCamino);	 
			}
}

	 private static void codificar(char[] input, Map<Character, String> diccionarioCodigos, String filePathOut,
				ArbolHuffman arbol) {

			EscritorBinario escritor = new EscritorBinario(filePathOut);

			// Serializar árbol de Huffman para recuperarlo posteriormente en la
			// descompresión.
			serializarArbol(arbol, escritor);

			// Escribir también el número de bytes del mensaje original (sin comprimir).
			escritor.escribirEntero(input.length);

			/////////////////////// TAREA1.6///////////////////////
			// Codificación usando el diccionario de códigos y escritura en el archivo de
			/////////////////////// salida.
			String codificacion = "";
			for (int i = 0; i < input.length; i++) {
				codificacion += diccionarioCodigos.get(input[i]);
			}

			for (int i = 0; i < codificacion.length(); i++) {
				if (codificacion.charAt(i)=='0') 
					escritor.escribirBit(false);
				else 
					escritor.escribirBit(true);
				
			}		
			escritor.cerrarFlujo();
			//////////////////////////////////////////////////////

			escritor.cerrarFlujo();
		}
	 
	 private static void serializarArbol(ArbolHuffman arbol, EscritorBinario escritor) {

			if (arbol.esHoja()) {
				escritor.escribirBit(true);
				escritor.escribirPalabra(arbol.getSimbolo()); // Escribir palabra de 8bits
				return;
			}
			escritor.escribirBit(false);
			serializarArbol(arbol.getIzquierdo(), escritor);
			serializarArbol(arbol.getDerecho(), escritor);
		}
	 
	 public static void descomprimir(String filePathIn, String filePathOut) {

			LectorBinario lector = new LectorBinario(filePathIn);
			EscritorBinario escritor = new EscritorBinario(filePathOut);

			ArbolHuffman arbol = leerArbol(lector);

			// Númerod e bytes a escribir
			int length = lector.leerEntero();
			
			// Decodificar usando el árbol de Huffman.
			for (int i = 0; i < length; i++) {
				ArbolHuffman x = arbol;
				while (!x.esHoja()) {
					boolean bit = lector.leerBit();
					if (bit)
						x = x.getDerecho();
					else
						x = x.getIzquierdo();
				}
				escritor.escribirPalabra(x.getSimbolo());
			}

			escritor.cerrarFlujo();
		}
	 
	 private static ArbolHuffman leerArbol(LectorBinario lector) {

			boolean esHoja = lector.leerBit();
			if (esHoja) {
				char simbolo = lector.leerPalabra();
				return new ArbolHuffman(simbolo, -1, null, null);
			} else {
				return new ArbolHuffman('\0', -1, leerArbol(lector), leerArbol(lector));
			}
		}
     //////////////////////////////////////////////////////
 }

