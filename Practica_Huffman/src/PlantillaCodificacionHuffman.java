import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;

import entrada_salida.EscritorBinario;
import entrada_salida.LectorBinario;
import estructuras_datos.ArbolHuffman;

/*********************************************************************************************
 * Ejecución: % Comprimir: java PlantillaCodificacionHuffman -c filePathIn
 * filePathOut % Descomprimir: java PlantillaCodificacionHuffman -d filePathIn
 * filePathOut
 * 
 * Utilidad: Permite la compresión/descompresión usando el algoritmo de Huffman
 * de un archivo de entrada hacia un archivo de salida.
 * 
 *
 *********************************************************************************************/

public class PlantillaCodificacionHuffman {

	// Constructor
	private PlantillaCodificacionHuffman() {

	}

	/*
	 * Se lee el archivo de entrada (filePathIn, a comprimir) como secuencia de
	 * palabras de 8 bits usando LectorBinario, después se codifica con el algoritmo
	 * de Huffman y el resultado se escribe usando la clase EscritorBinario hacia el
	 * archivo de salida (filePathOut, comprimido).
	 */
	public void comprimir(String filePathIn, String filePathOut) {

		LectorBinario lector = new LectorBinario(filePathIn);
		// Leer archivo de entrada y almacenar en una cadena
		StringBuilder sb = new StringBuilder();
		while (!lector.esVacio()) {
			char b = lector.leerPalabra();
			sb.append(b); // OJO! leerPalabra() devuelve una palabra
			// de 8 bits y el tipo char es de 16 bits
		}
		char[] input = sb.toString().toCharArray();

		// Generar tabla de frecuencias (freq) a partir del array de tipo char input.
		Map<Character, Integer> mapFrecuencias = new HashMap<Character, Integer>();
		int[] freq = new int[256];
		for (int i = 0; i < freq.length; i++) {
			freq[i] = 0;
		}
		for (int i = 0; i < input.length; i++) {
			freq[(int) input[i]] += 1;
		}
		for (int i = 0; i < freq.length; i++) {
			if (freq[i] != 0) {
				mapFrecuencias.put((char) i, freq[i]);
			}
		}

		//////////////////////////////////////////////////////

		// Construir árbol de Huffman.
		ArbolHuffman arbolHuffman = construirArbol(mapFrecuencias);
		// Construir diccionario de búsqueda;
		Map<Character, String> diccionarioCodigos = new HashMap<>();
		construirCodigos(diccionarioCodigos, arbolHuffman, "");
		// Codificar la trama (char[]input) usando el diccionario de códigos.
		codificar(input, diccionarioCodigos, filePathOut, arbolHuffman);
	}


	/*
	 * Construir arbol de Huffman a partir de la tabla de frecuencias. (Si se ha
	 * usado una estructura Map para albergar la tabla de frecuencias).
	 */
	private ArbolHuffman construirArbol(Map<Character, Integer> freq) {
		PriorityQueue<ArbolHuffman> cola = new PriorityQueue<>();
		Iterator<Character> it = freq.keySet().iterator();
		while (it.hasNext()) {
			Character letra = (Character) it.next();
			cola.add(new ArbolHuffman(letra, freq.get(letra), null, null));
		}
		
		ArbolHuffman arbolHuffman = null;
		while (cola.size() != 1) {
			ArbolHuffman aux1, aux2;
			aux1 = cola.poll();
			aux2 = cola.poll();
			int frecTotal = aux1.getFrecuencia() + aux2.getFrecuencia();
			arbolHuffman = new ArbolHuffman('\0', frecTotal, aux1, aux2);
			cola.add(arbolHuffman);
		}
		
		return arbolHuffman;
	}


	/*
	 * Construir diccionario de búsqueda -> Pares (símbolo,código). (Si se usa una
	 * estructura Map para albergar el diccionario de códigos).
	 */
	private void construirCodigos(Map<Character, String> diccionarioCodigos, ArbolHuffman arbol, String codigoCamino) {
		if (arbol.getDerecho() != null) {
			construirCodigos(diccionarioCodigos, arbol.getDerecho(), codigoCamino + "1");
		}
		if (arbol.getIzquierdo() != null) {
			construirCodigos(diccionarioCodigos, arbol.getIzquierdo(), codigoCamino + "0");
		} 
		if (arbol.esHoja()) {
			diccionarioCodigos.put(arbol.getSimbolo(), codigoCamino);
		}
		
	}
	/*
	 * Codificar la trama (char[]input) usando el diccionario de códigos y
	 * escribirla en el archivo de salida cuyo path (String filePathOut) se facilita
	 * como argumento. (Si se usa una estructura Map para albergar el diccionario de
	 * códigos).
	 */
	private void codificar(char[] input, Map<Character, String> diccionarioCodigos, String filePathOut,
			ArbolHuffman arbol) {

		EscritorBinario escritor = new EscritorBinario(filePathOut);

		// Serializar árbol de Huffman para recuperarlo posteriormente en la
		// descompresión.
		serializarArbol(arbol, escritor);

		// Escribir también el número de bytes del mensaje original (sin comprimir).
		escritor.escribirEntero(input.length);
		
		// Codificación usando el diccionario de códigos y escritura en el archivo de salida.
		String codificacion = "";
		for (int i = 0; i < input.length; i++) {
			codificacion += diccionarioCodigos.get(input[i]);
		}

		for (int i = 0; i < codificacion.length(); i++) {
			if (codificacion.charAt(i)=='0') 
				escritor.escribirBit(false);
			else 
				escritor.escribirBit(true);
			
		}		
		
		escritor.cerrarFlujo();
	}

	/*
	 * Serializar árbol de Huffman para recuperarlo posteriormente en la
	 * descompresión. Se escribe en la parte inicial del archivo de salida.
	 */
	private void serializarArbol(ArbolHuffman arbol, EscritorBinario escritor) {

		if (arbol.esHoja()) {
			escritor.escribirBit(true);
			escritor.escribirPalabra(arbol.getSimbolo()); // Escribir palabra de 8bits
			return;
		}
		escritor.escribirBit(false);
		serializarArbol(arbol.getIzquierdo(), escritor);
		serializarArbol(arbol.getDerecho(), escritor);
	}

	/*
	 * Se lee el archivo de entrada (filePathIn, a descomprimir) como secuencia de
	 * bits usando LectorBinario, después se descodifica usando el árbol final de
	 * Huffman y el resultado se escribe con la clase EscritorBinario en el archivo
	 * de salida (filePathOut, descomprimido).
	 */
	public void descomprimir(String filePathIn, String filePathOut) {

		LectorBinario lector = new LectorBinario(filePathIn);
		EscritorBinario escritor = new EscritorBinario(filePathOut);

		ArbolHuffman arbol = leerArbol(lector);

		// Númerod e bytes a escribir
		int length = lector.leerEntero();
		
		// Decodificar usando el árbol de Huffman.
		for (int i = 0; i < length; i++) {
			ArbolHuffman x = arbol;
			while (!x.esHoja()) {
				boolean bit = lector.leerBit();
				if (bit)
					x = x.getDerecho();
				else
					x = x.getIzquierdo();
			}
			escritor.escribirPalabra(x.getSimbolo());
		}

		escritor.cerrarFlujo();
	}

	private ArbolHuffman leerArbol(LectorBinario lector) {

		boolean esHoja = lector.leerBit();
		if (esHoja) {
			char simbolo = lector.leerPalabra();
			return new ArbolHuffman(simbolo, -1, null, null);
		} else {
			return new ArbolHuffman('\0', -1, leerArbol(lector), leerArbol(lector));
		}
	}

	public static void main(String[] args) {
		PlantillaCodificacionHuffman huffman = new PlantillaCodificacionHuffman();
		if (args.length == 3) { 
			if (args[0].equals("-c")) {
				huffman.comprimir(args[1], args[2]);
			} else if (args[0].equals("-d")) {
				huffman.descomprimir(args[1],args[2]);
			}
		}
	}

}
